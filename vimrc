" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

set nocompatible

" automatically reload vimrc when it's saved
autocmd BufWritePost vimrc so ~/.vim/vimrc

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" ========================
" Vundle stuff
" ========================
" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
" To update use :BundleInstall!
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
" let Vundle manage Vundle, required
Bundle 'gmarik/vundle'

Bundle 'bling/vim-airline'
Bundle 'scrooloose/nerdtree'
Bundle 'davidhalter/jedi-vim'
Bundle 'vim-scripts/Python-Syntax-Folding'
"Bundle 'vim-scripts/Efficient-python-folding'
Bundle 'scrooloose/syntastic'
Bundle 'majutsushi/tagbar'
Bundle 'altercation/vim-colors-solarized'
Bundle 'vim-scripts/vim-python-virtualenv'
"Bundle 'vim-scripts/pep8.git'
Bundle 'nvie/vim-flake8'
"Bundle 'klen/python-mode'
" Bundle 'tell-k/vim-autopep8' " broken package?
"Bundle 'vim-scripts/vim-misc'
"Bundle 'vim-scripts/easytags.vim'
"Bundle 'sjl/gundo'   // doesnt work?
Bundle 'vim-scripts/Gundo'


" pathogen
" execute pathogen#infect()
" call pathogen#infect()
" call pathogen#helptags()

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
 syntax on
endif


" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set autochdir       " automatically change current dir
set showcmd		    " Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden		    " Hide buffers when they are abandoned
set mouse=a		    " Enable mouse usage (all modes)
set autoindent
set copyindent
set smarttab        " insert tabs on the start of a line according to
                    " shiftwidth, not tabstop
set number		    " Enable line numbering
set hlsearch		" Search highlighting
set history=700
set undolevels=700
set t_Co=256        " 256 colors for airline and better looks
set nowrap
set title           " change the terminal's title
"set cpoptions=aABceFks
" syntax on
" filetype plugin indent on


" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
syntax enable
set background=dark
"colorscheme mustang
let g:solarized_termcolors=256
colorscheme solarized


" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

" jinja files syntax highlighting
au BufReadPost *.jinja set syntax=html

" Reselect visual block after indent/outdent
vnoremap < <gv
vnoremap > >gv

" Disable paste mode when leaving Insert Mode
"au InsertLeave * set nopaste

" Show text limit line
"if (v:version >=703)
"    set colorcolumn=80
"    hi ColorColumn ctermbg=darkgrey ctermfg=black
"endif

" better copy and paste
"set pastetoggle=<F2>
"set clipboard=unnamed

" tabs and spaces stuff
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab

" backup stuff
" /home/bartosz/BACKUP/vim
set backup
set backupdir=~/BACKUP/vim,.
set directory=~/BACKUP/vim/.swapfile,.
set writebackup

" ========================
" 	KEYBINDINGS 
" ========================
set <F1>=[[A
set <F2>=[[B
set <F3>=[[C
set <F4>=[[D

set <left>=OD
set <right>=OC
set <up>=OA
set <down>=OB

" Make double-<Esc> clear search highlights
nnoremap <silent> <Esc><Esc> <Esc>:nohlsearch<CR><Esc>

" tab navigation
nnoremap <silent> <C-n> :tabnext<CR>
nnoremap <silent> <C-p> :tabprevious<CR>
nnoremap <silent> <leader>t :tabnew<CR> 

" quicksave on ctrl-s in any mode
noremap <C-S> :update<CR>
vnoremap <C-S> <C-C>:update<CR>
inoremap <C-S> <C-O>:update<CR>

" Easy split navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" disable arrow keys
nnoremap <up> <nop>
nnoremap <down> <nop>
"nnoremap <left> <nop>
"nnoremap <right> <nop>
nnoremap <left> :lprevious<CR>
nnoremap <right> :lnext<CR>
nnoremap <M-left> :cprevious<CR>
nnoremap <M-right> :cnext<CR>


" Open NERDTree using F2
noremap <silent> <F2> :NERDTreeToggle<CR>

" autocorrect
noremap <F8> :SyntasticCheck<CR>
" tagbar
nnoremap <silent> <F9> :TagbarToggle<CR>

" gundo
nnoremap <F5> :GundoToggle<CR>

" ========================
"   PLUGINS
" ========================

" vim-latexsuite
filetype plugin on
set grepprg=grep\ -nH\ $*
filetype indent on
let g:tex_flavor='latex'


" easytags.vim
let g:easytags_file = "~/.vim/tags"
let g:easytags_async = 1
" only project specific files
let g:easytags_dynamic_files = 2
" scan recursively
let g:easytags_autorecurse = 0


" airline
" ------------------------
set laststatus=2
let g:bufferline_echo=0
let g:airline_detect_modified=1
let g:airline_detect_paste=1
let g:airline_theme='light'
" airline unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'


" YouCompleteMe
" ------------------------
" use it only for c/c++ files
let g:ycm_filetype_whitelist = { 'c': 1, 'cpp': 1 }
let g:ycm_filetype_specific_completion_to_disable = {}


" NERDTree
" ------------------------
" Open a NERDTree automatically when vim starts up if no files were specified
autocmd vimenter * if !argc() | NERDTree | endif

" Syntastic 
" ------------------------
let g:syntastic_python_checkers=['pyflakes']
"let g:syntastic_python_checkers=['pylint', 'pyflakes']
let g:syntastic_check_on_open = 0
let g:syntastic_aggregate_errors = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_always_populate_loc_list=0
let g:syntastic_auto_loc_list = 0

" Pymode
" ------------------------
" Turn off the plugin, since i dont use all of it
let g:pymode = 0
" fix interaction with syntactic
let g:pymode_lint_write = 0
" Setup default python options
let g:pymode_options = 0
" Enable pymode folding
" zo, zc - open/close one fold
" za - toggle fold one element
" zr, zM - fold/unfold all
let g:pymode_folding = 0
" Turn on code checking 
let g:pymode_lint = 0
" Enable pymode-motion
let g:pymode_motion = 0
" Turns on the documentation script
let g:pymode_doc = 0
" Bind keys to show documentation for current word (selection)
"let g:pymode_doc_bind = 'K'
" Enable automatic virtualenv detection 
let g:pymode_virtualenv = 0
" Set path to virtualenv manually
"let g:pymode_virtualenv_path = $VIRTUAL_ENV
" Turn on the run code script
let g:pymode_run = 0
" Auto open cwindow (quickfix) if any errors have been found
let g:pymode_lint_cwindow = 0

" Turn on code completion support in the plugin
let g:pymode_rope_completion = 0
" Turn on the rope script
let g:pymode_rope = 0


" jedi-vim
" --------
" There seems to be a bug with larger files
"let g:jedi#show_function_definition = 0
let g:jedi#show_call_signatures = 0
let g:jedi#popup_on_dot = 0


" vim-flake8
" ----------
" ignore line too long
let g:flake8_ignore="E501"
